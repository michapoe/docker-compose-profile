#!/bin/sh

WAIT=$1
PORT=$2
MESSAGE=$3

echo "Listening on Port $PORT for $WAIT seconds ..."
nc -l $PORT &
echo "Message: $MESSAGE"
sleep $WAIT
kill $(jobs -p)

#!/bin/bash

test() {
  TEST="$1"
  CMD="$2"

  echo "###"
  echo "### Executing Test '$TEST' - '$CMD'"
  bash -c "$CMD"
  RC=$?

  if [ $RC -eq 0 ] ; then
    echo "Passed '$TEST'"
  else
    echo "Failed '$TEST' - RC: $RC"
    failed=$(($failed + $?))
  fi

  echo "###"
  echo ""
}

# main

failed=0

DCP=$(which dcp)
echo "Which dcp: $DCP"

test "List services, profiles, command aliases" \
  "dcp -ls -lp -lca"
test "List profiles, command aliases details" \
  "dcp -lp -lca --details"
test "Perform a slow startup using profile 'slowly' and command alias 'upd'" \
  "dcp -p slowly upd"
test "Stop slowly started stack" \
  "dcp -p slowly down"
test "Only perform a dry run of a single step 'db-first' of profile 'slowly'" \
  "dcp -p slowly upd --step db-first --dry"

test "Performing wait conditions using profiles default command" \
  "dcp -p wait-4-server"
docker-compose down

test "Performing wait condition (short syntax) using profiles default command" \
  "dcp -p short-wait-4-server"
docker-compose down

test "Perform execution within separated stack" \
  "dcp -p stack-name"
docker-compose down

if [[ $failed -eq 0 ]] ; then
  echo "All tests passed!"
  exit 0
else
  echo "$failed tests failed :("
  exit 1
fi
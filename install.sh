#!/usr/bin/env bash

source_dir="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"

# Functions
setup_colors() {
  ncolors=$(tput colors 2> /dev/null)
  if [[ -n "$ncolors" ]] && [[ $ncolors -ge 8 ]] ; then
    COLOR_DEBUG=$(tput setaf 7)
    COLOR_SUCCESS=$(tput setaf 2)
    COLOR_INFO=$(tput setaf 4)
    COLOR_ERROR=$(tput setaf 5)
    COLOR_RESET=$(tput sgr0)
  fi
}

log_color() {
  COLOR=$1
  shift
  if [[ $# -gt 1 ]] ; then
    ARG=$1
    shift
  fi
	echo $ARG "${COLOR}$*${COLOR_RESET}"
	unset ARG
}
log_debug() {
  if [[ -n "$DCP_DEBUG" ]] ; then
    log_color "${COLOR_DEBUG}" "$@"
  fi
}
log_success() {
  log_color "${COLOR_SUCCESS}" "$@"
}
log_info() {
  log_color "${COLOR_INFO}" "$@"
}
log_error() {
  log_color "${COLOR_ERROR}" "$@"
}

init_env() {
  if [ $# -gt 0 ]; then
    install_dir=$1
    log_info "Detected custom install dir argument - installing to $install_dir"
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    install_dir=${HOME}/.local/share/dcp
    log_info "Detected Linux - installing to $install_dir"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    install_dir=${HOME}/bin/dcp
    log_info "Detected macOS - installing to $install_dir"
    log_info "If you are facing build problems think about disabling buildkit for docker by appending 'export DOCKER_BUILDKIT=0' and 'export COMPOSE_DOCKER_CLI_BUILD=0' to your rc file"
  else
    log_error "Unsupported OS: $OSTYPE - please provide install directory as first parameter"
    return 1
  fi
}

check_requirements() {
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    which yq >/dev/null || snap_install yq "Package yq is missing and has to be installed"
    which jq >/dev/null || apt_install jq "Package jq is missing and has to be installed"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    which yq >/dev/null || brew_install yq "Package yq is missing and has to be installed"
    which jq >/dev/null || brew_install jq "Package jq is missing and has to be installed"
    declare -A check_bash_version >/dev/null || macos_install_bash4
    which timeout >/dev/null || macos_install_timeout
  elif [[ "$OSTYPE" == "linux-musl"* ]]; then
    log_info "Assuming installer is running in CI environment."
  else
    log_error "Unsupported OS: $OSTYPE"
    return 1
  fi
}

snap_install() {
  PACKAGE="$1"
  MSG="$2"

  log_info "$MSG"
  sudo snap install $PACKAGE
}

apt_install() {
  PACKAGE="$1"
  MSG="$2"

  log_info "$MSG"
  sudo apt-get install -y $PACKAGE
}

brew_install() {
  PACKAGE="$1"
  MSG="$2"

  log_info "$MSG"
  brew install $PACKAGE
}

macos_install_bash4() {
  log_info "Your current bash version ${BASH_VERSION} does not support associative arrays--perhaps it is a bit outdated."
  brew_install bash "Package bash4 is missing and has to be installed"
}

macos_install_timeout() {
  log_info "No timeout binary found."
  brew_install coreutils "Package coreutils is missing and has to be installed"
  log_info "Creating symbolic link named timeout pointing to gtimeout"
  sudo ln -s /usr/local/bin/gtimeout /usr/local/bin/timeout
}

install_base() {
  log_info "Installing docker-compose-profile to '${install_dir}'"

  mkdir -p ${install_dir}
  install -m 755 ${source_dir}/src/dcp ${install_dir}
  install -m 755 ${source_dir}/src/*.bash ${install_dir}
  install -m 644 ${source_dir}/src/*.properties ${install_dir}
  install -m 644 ${source_dir}/README.md ${install_dir}
  cp -a ${source_dir}/example ${install_dir}/
  cp -a ${source_dir}/resources ${install_dir}/
}

build_docker_image() {
  log_info "Building required docker image"
  (cd ${source_dir}/resources && sh install-docker-wait-image.sh)
}

install_shell_integration() {
  if [[ "$SHELL" =~ ^/bin/zsh ]]; then
    shell_rc_file="$HOME/.zshrc"
    log_info "Detected default shell zsh - adding settings to $shell_rc_file"
  elif [[ "$SHELL" =~ ^/bin/bash ]]; then
    shell_rc_file="$HOME/.bashrc"
    install_bash_completion=true
    log_info "Detected default shell bash - adding settings to $shell_rc_file"
  elif [[ "$SHELL" =~ ^/bin/sh ]]; then
    shell_rc_file="$HOME/.profile"
    log_info "Detected default shell sh - adding settings to $shell_rc_file"
  elif [[ "$SHELL" =~ ^/bin/ash ]]; then
    shell_rc_file="$HOME/.profile"
    log_info "Detected default shell ash - adding settings to $shell_rc_file"
  else
    log_error "Installer right now only supports zsh and ash/bash--your current default shell: $SHELL. Please consult README.md how to install."
    return 1
  fi

  grep -q "# docker-compose-profile" ${shell_rc_file}
  if [ $? -eq 0 ] ; then
    log_info "Shell integration seems to be already installed - skipping installation"
    return
  fi

  log_info "Adding shell integration"
  # expand ${install_dir}
  cat << EOF >> ${shell_rc_file}
# docker-compose-profile
DCP_DIR="${install_dir}"
EOF
  # don't expand ${PATH} and ${DCP_DIR}
  cat << 'EOF' >> ${shell_rc_file}
PATH=${PATH}:${DCP_DIR}
EOF

  if [ -n "$install_bash_completion" ]; then
    log_info "Adding bash auto completion"
    cat << 'EOF' >> ${shell_rc_file}
    if [ -f ${DCP_DIR}/dcp-completion.bash ] ; then
        . ${DCP_DIR}/dcp-completion.bash
    fi
EOF
  else
    log_info "Skipping bash auto completion--as default shell seems not be bash"
  fi

  log_info ""
  log_info "###"
  log_info "# In case You are using any tool which requires to be found at the end of ${shell_rc_file} - fix this file manually!"
  grep -q "SDKMAN" ${shell_rc_file} 2> /dev/null
  if [ $? -eq 0 ] ; then
    log_info "# I found an installation of 'SDKMAN' which has this special requirement."
    log_error "# WARNING So please fix ${shell_rc_file} accordingly!"
  fi
  log_info "###"
  log_info ""
}

setup_colors
init_env $* || exit $?
check_requirements || exit $?
install_base || exit $?
install_shell_integration $* || exit $?
build_docker_image || exit $?

log_success "docker-compose-profile installed successfully."
#!/usr/bin/env bash

_find_profile() {
  args=( $@ )
  for i in "${!args[@]}"; do
    if [[ "${args[$i]}" == "-p" ]] ; then
      next=$(($i + 1))
      profile=${args[${next}]}
      return
    fi
  done

  DCP_PROFILE_DEFAULT="default"
  DIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"
  [[ -r $DIR/dcp.properties ]] && . $DIR/dcp.properties
  profile=$DCP_PROFILE_DEFAULT
}

_dcp_completion() {
  COMPREPLY=()

  local cur=${COMP_WORDS[COMP_CWORD]}
  local prev=${COMP_WORDS[COMP_CWORD-1]}
  local options=("-h" "--help" "-p" "--profile" "-lp" "--list-profiles" "-ls" "--list-services" "-lca" "--list-command-aliases" "--step" "--continue" "--dry" "--debug" "--trace" "--details")

  if [[ "$prev" == "-p" ]] ; then
    [[ -r dcp.yml ]] || return 0
    DCP_config_json=$(yq -o=json eval dcp.yml)
    IFS=$'\n' read -rd '' -a profiles <<< "$(jq -r '.profiles | keys[]' <<< $DCP_config_json)"
    COMPREPLY=( $(compgen -W "${profiles[*]}" -- "$cur") )
  elif [[ "$prev" == "--step" ]] || [[ "$prev" == "--continue" ]] ; then
    [[ -r dcp.yml ]] || return 0
    _find_profile "${COMP_WORDS[*]}"
    DCP_config_json=$(yq -o=json eval dcp.yml)
    profile_json=$(jq -r ".profiles[\"${profile}\"]" <<< $DCP_config_json)
    IFS=$'\n' read -rd '' -a steps <<< "$(jq -r .steps[]?.label <<< $profile_json)"
    COMPREPLY=( $(compgen -W "${steps[*]}" -- "$cur") )
  elif [[ "$cur" == -* ]] || [[ -z "$cur" ]] ; then
    # Aliases
    COMPREPLY=( $(compgen -W "${options[*]}" -- "$cur") )
    IFS=$'\n' read -rd '' -a aliases <<< "$(jq -r '.command.aliases | keys[]' <<< $DCP_config_json)"
    COMPREPLY=( "${COMPREPLY[@]}" "${aliases[@]}" )
  fi
}

complete -F _dcp_completion dcp
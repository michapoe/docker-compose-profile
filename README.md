# Docker Compose Profile

## TL;TR

Solution to start a `docker-compose` environment smoothly--optionally using groups of services and wait conditions.

## Motivation

In extensive `docker-compose` environments with many services there are always different requirements.
Sometimes services have to be started in groups one after the other, for example to reduce the load on the computer
by starting individual services to make them manageable in total. 
Sometime different overrides must be used or services have to be excluded.

### Scenario soft start of multiple services

Some services really eat up memory and IO when they start. So it would be quite nice to start them in well-defined
groups and have some waiting time, connection tests, or ensured log message before others to ramp up.
I.e. start a huge database fist later some authentication services and api-gateway and finally your services using them.
With `docker-compose` there is actually no solution to do so.

When You use `dcp` You can define some steps and delays wait conditions.
So actually You are grouping services together to start in each single step.
Now the peek of the system load can be drastically reduced.

### Scenario isolation of service in IDE

So let's assume You are a software developer in a bigger environment. May be there are even several teams around
working on different services which have to be orchestrated all together to build app the software solution.
All services can be run as a `docker` container. So usually You have a `docker-compose` setup to test your stuff locally.
Now let's assume You want to develop and test some changes fully integrated with all the other services.
Usually You are using a nice IDE for that purpose. Now when You are starting your `docker-compose` environment You
have to exclude the service You are currently working on within your IDE. Further more You have to tell the other
services where (usually hostname and port) they can find that service. That is the moment where a first
`docker-compose.override` jumps in. So easy for the moment.
Sadly You found a bug on another service and You decide to fix that one immediately. So You want to extract that one
too from your `docker-compose` environment. Another `docker-compose.override` jumps in the scene.
It might become a bit unhandy to start your stack from terminal as You have to use different overrides and
command line becomes longer and longer.

Now what is the difference now, using `dcp`?
You can define a profile for excluding the service and define the override to use (to give the other services
some hints, where to find the service running in your IDE). The command line becomes where readable and short.
It is just like `dcp -p isolated`.

## Solution

Here a bash script is provided, which reads configuration profiles from a YAML file,
to finally apply them to a `docker-compose` environment.
The `docker-compose.yml` is read to determine the defined services.

### Configuration

There must be a file named `dcp.yml` in the current directory when the script is executed,
which has the following structure:
```yaml
command:
  aliases:
    upd:
      command: "up -d"
profiles:
  default:
  slowly:
    steps:
      - label: backends
        only:
          - database
          - minio
        wait:
          - service: database
            method: log
            timeout: 5
            message: database system is ready to accept connections
      - label: core
        only:
          - identity-provider
          - api-gateway
        wait:
          - 5@tcp://api-gateway:8080
        delay: 2s
      - label: apps
  isolated:
    command: upd
    overrides:
      - docker-compose.isolated-4-ide.yml
    excludes:
      - fancyapp1
      - fancyapp2
  custom-stack-name:
    command: upd
    stack: my-stack-name
```
#### Profiles

An attribute `profiles` must be created in the root, which contains a list of profiles.

A profile can have the following attributes (all optional):
- `overrides`: optional list of override files for `docker-compose`
- `command`: optional command to run, when no command is given on terminal - see "Usage of this script" below
- `excludes`: List of services that are always excluded in this profile.
- `stack`: optional name of stack to use for this profile - this will actually use `docker-compose`s `project-name` feature.
  See `docker-compose -h` and watch out for option `-p` or `--project-name`.\
  This feature is very handy if You have to use volumes but want to separate them between profiles.
  Volume names are usually derived from the `docker-compose` project name (which will be used by `stack` option).\
  Please always use `dcp` to shut down profiles using the `stack` feature! 
- `steps`: Individual steps that are executed when a `docker-compose' command is executed.
   Each step can have the following attributes:
   - `label`: mandatory specification of the name of the step 
     (no minus signs are allowed here, only characters and numbers, possibly underscore)
   - `only`: optional list of services that are exclusively dealt with in this step.
     Enables you to start a subset of services.
   - `delay`: optional delay after the execution of the step. Please refer to `man sleep` for the syntax.
   - `wait`: optional list of wait conditions being performed after the step is executed.
     Execution order is same order as they are defined which actually should not matter at all, as anyhow
     all conditions have to be fulfilled as a precondition for executing the next step.
     The attributes of a wait condition are:
     - `service`: the name of the service to apply the condition
     - `timeout`: a timeout in seconds - reaching this amount of time leads the wait condition to fail
     - `method`: the method how to perform the condition.
       The following waiting methods are supported:
       - `tcp` This method trys to connect via tcp.\
         You have to provide an additional attribute `port.`
         This also works for services which don't map their parts to host.
         The connection will be established from inside the stack by running a helper docker image
         which itself then connects to the service.
       - `log` This method waits till the service produces a specific log message.\
         You have to provide an additional attribute `message`.  
     - `port`: the port number to connect to - only relevant for method `tcp`.
     - `message`: the log message to wait for - only relevant for method `log`.
     
     There are two ways to define a wait condition:\
     *Long syntax*: Like described above providing attributes as usual in a yaml format.\
     *Short syntax*: Compact mode in (modified) UIR style:\
     `<wait-method>@<timeout>://<service>:<special-args-for-wait-method>`
     You can also mix them defining a first wait condition in long syntax and another one
     using short syntax.

**Hint** on using `delay` and `wait` together: Execution order is first delay than wait.

In the above example, the `default` profile means that no changes are made to a native
is done by `docker-compose`. But should be mentioned for the sake of completeness.

The `slowly` profile is an example of how to start services slowly and gently.
So here in the first step `backends` only starts the `database` and `minio` service and then waits
till a tcp connection to service database can be established within 5 seconds on port 5432.
In the next step `core` central services will be started and then waits 2 seconds.
In the third and last step `apps` no attribute is set, which conversely means that there is no restriction
on the services to be handled and thus the rest is started. Excludes would affect the services to start.

The `isolated` profile is an example of how to take a service out of the environment in an isolated manner to
for example in the IDE (for easy and fast development). For this purpose this profile has
a special override file, which defines extra hosts if necessary, or extends ports to the outside. At the same time
the isolated services are completely excluded from execution (since they are supposed to run in the IDE).

#### Command Aliases

Furthermore, aliases can be defined for frequently used `docker-compose` commands.
These must be defined under the root as `command.aliases`.
Each element represents an alias and has a `command` attribute, which defines the command passed to `docker-compose`.
is an argument to be made.

Note: A separate attribute was deliberately chosen instead of the direct specification
of the command to make it easier to implement extensions in the future while remaining backwards compatible.

## Usage of this script

All this is operated with a central script `dcp`.
You can get help by calling it with argument `-h` or `--help`.

The syntax is...

```
dcp [option...] [command]
  option:
    -h|--help
          Output of this help
    -p|--profiles <profile-name>
          Using the profile <profile-name>
    -v|--version"
          Show version of dcp and exit"
    -u|--update"
          Update if version different from the one installed is available"
    -ls|--list-services
          Lists all defined services (from docker-compose.yml)
    -lp|--list-profiles
          Lists all defined profiles (from dcp.yml)
    -lca|--list-command-aliases
          Lists all defined command aliases  
    --details"
          Show more details when using a list option like -lp or -lca"
    --step
          Only perform this step
    --continue <step-name>"
          Continue from given step <step-name> of given profile"
    --dry
          Test run without execution
    --debug
          debug mode
    --trace"
          Trace mode - also show JSON conversion results"
  command
    up, down...
          Any command with arguments for docker-compose (in quotation marks if necessary),
          or any command alias from dcp.yml
```

The specification of a profile is not necessary. Then a profile called `default` is used.
This can be changed using the `dcp.properties`.

**Example** to test the stepwise start of a stack:
```
dcp -p slowly --dry --debug upd
```
Test run (`--dry`) with output of debug messages (`--debug`) for profile `slowly` (`-p slowly`).\
The command alias `upd` is used, which will resolve to `up -d`.

```
dcp -p slowly --dry --debug "up -d"
```
If you would not use this alias, the command would have to be put in quotation marks, because it contains further arguments.

**Example** to perform a single step:
```
dcp -p slowly --step core "up -d"
```
Execute step `core` (`--step core`) for profile `slowly` (`-p slowly`).\
Here, for example, only the two services `identity-provider` and `api-gateway` from the above example are covered.

**Example** to perform a profile with its default command:
```
dcp -p slowly
```
Execute profile `slowly` (`-p slowly`). As there is no command given to execute the profiles default command is consulted.\
In the example above this is defined to `upd` which actually is an alias resolving to `up -d`.
So the final command being executed will be `dcp -p slowly "up -d"`.

Try it out uning a dry run.
```
dcp -p slowly --dry
```

**Example** to use a custom stack:
```
dcp -p custom-stack-name
```
Execute profile `custom-stack-name` (`-p custom-stack-name`). The default command will be consulted.\
In the example above this is defined to `upd` - same like the example before.
As this profile defines a `stack` everything will be run within that `docker-compose` project name.

**Example** to continue from given step (i. e. after a failure):
```
dcp -p slowly --continue core
```
Execute profile `slowly` (`-p slowly`) and continue at step `core` (`--continue core`). The default command will be consulted.\
In the example above there are the following steps defined: `backends`, `core`, and `apps`.
Let's assume there was a problem starting a service in step `core`. You fixed that problem and know want to continue
starting all other services but don't want to rerun the previously executed steps again. Now the `--continue` becomes very handy.
It will skip all steps that would usually be executed before printing a short message and finally continue with step `core`.

**Example** detailed output of the defined profiles:
```
dcp -lp --details
```
would produce the following output in this example:
```
Defined profiles: default isolated slowly
```
**Example** output if defined services:
```
dcp -ls
```
would output for this example of `docker-compose.yml`
```yaml
version: '3'

services:
  app1:
    image: hello-world
  app2:
    image: hello-world

  database:
    image: hello-world
  minio:
    image: hello-world
  id-broker:
    image: hello-world
  api-gateway:
    image: hello-world
```
the following:
```
Defined services: api-gateway app1 app2 database minio id-broker
```

A **special treatment** is given to the command `down`, because you do not have to proceed in steps here.
Here all steps of the specified profile are ignored.
You could also use `docker-compose down` natively (which would not handle excludes).

Usually you will only control the boot of a stack with the tool.

## Installation

Clone and install using these commands:
```
git clone https://gitlab.com/michapoe/docker-compose-profile.git
cd docker-compose-profile
./install.sh
```

Some details about `dcp` installer:

`dcp` has to be reachable within your `$PATH`.
Therefore, the install script will add its installation directory.

To use the bash *auto completion* `dcp-completion.bash` has to be sourced.
This will be done automatically when your default shell is bash as long this is not already the case.\
**Warning**: It seems that there are some tools out there which need to be found as the last entry in your `.bashrc`.
If this is the case please fix your `.bashrc` **manually**!
There will also be a hint printed by the installer. Watch out!

There is also a `dcp.properties` file installed.
It is used to define a default profile in case `dcp` is called without.
It also contains the current version number--don't change it manually.

## Uninstallation

Find the following lines in your `.bashrc`:
```
# docker-compose-profile
DCP_DIR=...
if [ -f ${DCP_DIR}/dcp-completion.bash ]; then
    . ${DCP_DIR}/dcp-completion.bash
fi
```
Remove them completely.\
Also remove the whole `dcp` installation directory `$DCP_DIR`.

## Requirements

- Linux (only tested on Ubuntu)
  - `yp` - is installed with `snap` if necessary.
  - `jq` - has anyhow everyone installed but if necessary will be installed with `apt-get`.

- macOS
    - `yp` - is installed with `brew` if necessary.
    - `jq` - has anyhow everyone installed but if necessary will be installed with `brew`.
    - `bash` version >= 4 - macOS has bash 3.x installed but we need a version >= 4. Will be installed with `brew`.
    - `timeout` - is installed with `brew` (coreutils -> gtimeout) and linked to `/usr/local/bin/timeout` if necessary.

## Supported shells

Currently, installer only supports sh/ash/bash and zsh. However `dcp` will run using `bash`.
If You are using a different default shell please follow the readme to install manually.

## Example

You can find a tiny example in folder [example](./example).

## Notes

As I am actually not where good in shell scripting any hint and help is very welcome.
Please feel free to contact my for any question or feature request.
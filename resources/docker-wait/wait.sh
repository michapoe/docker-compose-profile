#!/bin/sh

#
# Docker Wait For
#

# exit if rc!=0
set -e

APPNAME=$( basename "$0" )

# Functions
log_color() {
  PREFIX=$1
  shift
  if [ $# -gt 1 ] ; then
    ARG=$1
    shift
  fi
	echo $ARG "${PREFIX}$*"
	unset ARG
}
log_debug() {
  if [ -n "$DWF_DEBUG" ] ; then
    log_color "DEBUG   " "$@"
  fi
}
log_success() {
  log_color "SUCCESS " "$@"
}
log_info() {
  log_color "INFO    " "$@"
}
log_error() {
  log_color "ERROR   " "$@"
}

usage() {
  log_info "Usage $APPNAME [option...]"
  log_info "  option:"
  log_info "    -h|--help"
  log_info "          Show this help"
  log_info "    -t|--timeout"
  log_info "          Timeout in seconds"
  log_info "    -s|--service"
  log_info "          Service to check"
  log_info "    -p|--port"
  log_info "          Port of service to check"
  log_info "    --debug"
  log_info "          Debug mode - in case of problems show some internal details and watch for errors"
  log_info "Will return 0 if service is up within timeout otherwise 99."
  log_info "Will return >0 and <99 for other errors."
  log_info "Example"
  log_info "  $APPNAME -t 10 -s database -p 1521"
  log_info "  Check service 'database' (-s database) on port '1521' (-p 1521) for max 10 seconds (-t 10)"
}

default_args() {
  DWF_TIMEOUT=30
}

parse_args() {
  log_debug "Arguments: $*"
  argscnt=$#
  while [ "$#" -gt 0 ]; do
    arg=$1
    case $arg in
      -h|--help)
        usage
        return 1
        ;;
      -t|--timeout)
        DWF_TIMEOUT="$2"; shift
        ;;
      -s|--service)
        DWF_SERVICE="$2"; shift
        ;;
      -p|--port)
        DWF_PORT="$2"; shift
        ;;
      --debug)
        DWF_DEBUG=true;
        ;;
      *)
        log_error "Unknown argument $arg"
        usage
        return 1
        ;;
    esac
    shift
  done
  log_debug "$argscnt Arguments read successfully."
}

check_args() {
  if [ -z "$DWF_SERVICE" ] ; then
    log_error "No service given"
    usage
    return 1
  fi
  if [ -z "$DWF_PORT" ] ; then
    log_error "No port given"
    usage
    return 1
  fi
}

process() {
  log_info -n "Waiting for '$DWF_SERVICE' on port '$DWF_PORT' for max '$DWF_TIMEOUT' seconds: "
  seconds=0
  while [ "$seconds" -lt "$DWF_TIMEOUT" ] && ! nc -z -w1 "$DWF_SERVICE" "$DWF_PORT"
  do
    log_debug -n "."
    seconds=$((seconds+1))
    sleep 1
  done

  if [ "$seconds" -lt "$DWF_TIMEOUT" ]; then
    log_success " up!"
    exit 0
  fi

  log_error " unable to connect" >&2
  exit 99
}

# main
default_args
parse_args "$@" || exit $?
check_args || exit $?
process || exit $?

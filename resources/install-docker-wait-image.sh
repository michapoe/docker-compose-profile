#!/bin/sh

DIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"
[ -r $DIR/dcp.properties ] && . $DIR/dcp.properties || (echo "ERROR $DIR/dcp.properties not found" ; exit 1)

echo "Properties: "
cat $DIR/dcp.properties

echo "Building docker-wait:$DCP_DOCKER_WAIT_VERSION ..."
docker build ${DIR}/docker-wait -t docker-wait:$DCP_DOCKER_WAIT_VERSION
echo "Ok."